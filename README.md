# React Web Component


## Usage


    import { ReactWebComponent } from '@karsegard/react-web-component';
    import MyComponent from './MyComponent'


    customElements.define('my-custom-tag', ReactWebComponent(MyComponent));

## In browser

This requires several script imports 

    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/17.0.2/umd/react.production.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react-dom/17.0.2/umd/react-dom.production.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prop-types/15.8.1/prop-types.min.js" ></script>
    <script  src="https://unpkg.com/@karsegard/react-web-component@1.0.3/dist/reactWebComponent.umd.js" defer></script>

then you can import your component

    <script  src="./dist/k-select.umd.js" defer></script>