
import React from 'react';
import ReactDOM from "react-dom";
import htmlToReact from 'html-to-react';
import { is_type_string, is_type_array, is_type_object } from '@karsegard/composite-js'

export const ReactWebComponent = (Component, options = { shadow: false, shadowMode: 'open' }) => (
    class WebComponent extends HTMLElement {
        constructor() {
            super();
            if (options.shadow) {
                this.attachShadow({ mode: options.shadowMode || 'open' });
            }
            this.observer = new MutationObserver(() => this.update());
            this.observer.observe(this, { attributes: true });
            this.styles = [];
        }

        connectedCallback() {
            this._innerHTML = this.innerHTML;
            this.mount();
        }

        adoptedCallback() {
            console.log('adopted');
        }
        disconnectedCallback() {
            this.unmount();
            this.observer.disconnect();
        }

        mount() {
            this.configureShadow();

            const props = {
                ...this.getProps(this.attributes, Component.propTypes),
                ...this.getEvents(Component.propTypes),
                children: this.parseHtmlToReact(this._innerHTML)
            };
            ReactDOM.render(<Component {...props} />, this.shadowRoot || this);

            if (this.shadowRoot) {
                this.styles.map(style => {
                    var cssFile = document.createElement('link');

                    cssFile.rel = "stylesheet";  // or path for file {themes('/styles/mobile.css')}
                    cssFile.href = style;

                    this.shadowRoot.prepend(cssFile);
                })
            }

        }



        unmount() {

            ReactDOM.unmountComponentAtNode(this.shadowRoot || this);
        }

        update() {
            this.unmount();
            this.mount();
        }

        parseHtmlToReact(html) {
            return html && new htmlToReact.Parser().parse(html);
        }

        configureShadow() {
            let shadowProps = this.getShadowOptions(this.attributes);
            if (shadowProps) {
                if (shadowProps.shadowenabled && !this.shadowRoot) {
                    this.attachShadow({ mode: shadowProps.shadowmode || 'open' });

                    if (shadowProps.shadowstyles) {

                        let styles = shadowProps.shadowstyles;
                        if (is_type_array(styles)) {
                            this.styles = styles;
                        } else if (is_type_string(styles)) {
                            this.styles = [styles];

                        }
                    }

                }
            }
        }

        getShadowOptions(attributes) {
            return [...attributes]
                .filter(attr => attr.name !== 'style')
                .filter(attr => /^shadow(.*)/.test(attr.name) != false)
                .map(attr => {
                    let value = attr.value;
                    if (value === 'true' || value === 'false')
                        value = value == 'true';
                    else if (/^{.*}/.exec(value))
                        value = JSON.parse(value);
                    else if (/^\[.*\]/.exec(value))
                        value = JSON.parse(value);
                    return {
                        name: attr.name,
                        value
                    };
                })
                .reduce((props, prop) =>
                    ({ ...props, [prop.name]: prop.value }), {});
        }



        getProps(attributes, propTypes) {
            propTypes = propTypes || {};
            return [...attributes]
                .filter(attr => attr.name !== 'style')
                .filter(attr => /^shadow(.*)/.test(attr.name) == false)
                .map(attr => this.convertProps(propTypes, attr.name, attr.value))
                .reduce((props, prop) =>
                    ({ ...props, [prop.name]: prop.value }), {});
        }


        convertProps(propTypes, attrName, attrValue) {
            const propName = Object.keys(propTypes)
                .find(key => key.toLowerCase() == attrName);
            if (attrName == 'class') {
                attrName = 'className';
            }
            let value = attrValue;
            if (attrValue === 'true' || attrValue === 'false')
                value = attrValue == 'true';
            else if (!isNaN(attrValue) && attrValue !== '')
                value = +attrValue;
            else if (/^{.*}/.exec(attrValue))
                value = JSON.parse(attrValue);

            return {
                name: propName ? propName : attrName,
                value: value
            };

        }



        getEvents(propTypes) {
            return Object.keys(propTypes)
                .filter(key => /on([A-Z].*)/.exec(key))
                .reduce((events, ev) => {
                    return {
                        ...events,
                        [ev]: args => {
                            return this.dispatchEvent(new CustomEvent(ev, { detail: args }))
                        }
                    }
                }, {});
        }


    });